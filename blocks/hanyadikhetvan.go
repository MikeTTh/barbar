package blocks

import (
	"encoding/json"
	"fmt"
	"gitlab.com/MikeTTh/barbar"
	"io/ioutil"
	"net/http"
	"time"
)

type HanyadikhetvanBlock struct {
	barbar.I3Block
}

type api struct {
	Week int
}

func (h HanyadikhetvanBlock) Run(c chan barbar.I3Block) error {
	for {
		r, e := http.Get("https://hanyadikhetvan.attiss.eu/api")
		if e != nil {
			time.Sleep(time.Minute)
			continue
		}

		b, e := ioutil.ReadAll(r.Body)
		if e != nil {
			time.Sleep(time.Minute)
			continue
		}

		var a api
		e = json.Unmarshal(b, &a)
		if e != nil {
			time.Sleep(time.Minute)
			continue
		}

		c <- barbar.I3Block{
			FullText: fmt.Sprintf("%d. hét van", a.Week),
		}

		time.Sleep(time.Hour)
	}
}
