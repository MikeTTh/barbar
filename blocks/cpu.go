package blocks

import (
	"fmt"
	"github.com/elastic/go-sysinfo"
	"gitlab.com/MikeTTh/barbar"
	"time"
)

type CpuBlock struct {
	barbar.SmartBlock
}

func (cp CpuBlock) Run(c chan barbar.I3Block) error {
	lastIdle, lastTotal := time.Duration(0), time.Duration(0)

	for {
		host, err := sysinfo.Host()
		if err != nil {
			return err
		}

		cpu, e := host.CPUTime()
		if e != nil {
			return e
		}

		i, t := cpu.Idle, cpu.Total()
		idle, total := i-lastIdle, t-lastTotal
		lastIdle, lastTotal = i, t

		percent := float64(total-idle) / float64(total) * 100

		c <- barbar.I3Block{
			FullText: fmt.Sprintf("CPU: %5.2f%%", percent),
			Color:    defaultGradient(percent).String(),
		}
		time.Sleep(time.Second)
	}
}
