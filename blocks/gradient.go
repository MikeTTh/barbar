package blocks

import (
	"fmt"
	"sort"
)

var (
	red = color{
		R: 0xf4,
		G: 0x43,
		B: 0x36,
	}
	yellow = color{
		R: 0xff,
		G: 0xeb,
		B: 0x3b,
	}
	green = color{
		R: 0x66,
		G: 0xbb,
		B: 0x6a,
	}
)

type color struct {
	R float64
	G float64
	B float64
}

func (c color) String() string {
	return fmt.Sprintf("#%02x%02x%02x", uint8(c.R), uint8(c.G), uint8(c.B))
}

type gradientNode struct {
	Place float64
	Color color
}

func getGradientResolver(nodes []gradientNode) func(f float64) color {
	sort.Slice(nodes, func(i, j int) bool {
		return nodes[i].Place < nodes[j].Place
	})

	return func(f float64) color {
		if f < nodes[0].Place {
			return nodes[0].Color
		}
		for i := 1; i < len(nodes); i++ {
			if f < nodes[i].Place {
				d := (f - nodes[i-1].Place) / (nodes[i].Place - nodes[i-1].Place)
				r := nodes[i-1].Color.R + d*(nodes[i].Color.R-nodes[i-1].Color.R)
				g := nodes[i-1].Color.G + d*(nodes[i].Color.G-nodes[i-1].Color.G)
				b := nodes[i-1].Color.B + d*(nodes[i].Color.B-nodes[i-1].Color.B)

				return color{
					R: r,
					G: g,
					B: b,
				}
			}
		}
		return nodes[len(nodes)-1].Color
	}
}

var defaultGradient = getGradientResolver([]gradientNode{
	{
		Place: 50,
		Color: green,
	},
	{
		Place: 80,
		Color: yellow,
	},
	{
		Place: 90,
		Color: red,
	},
})
