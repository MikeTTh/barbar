package blocks

import (
	"fmt"
	"gitlab.com/MikeTTh/barbar"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"time"
)

var batGrad = getGradientResolver([]gradientNode{
	{
		Place: 10,
		Color: red,
	},
	{
		Place: 40,
		Color: yellow,
	},
	{
		Place: 60,
		Color: green,
	},
})

type BatteryBlock struct {
	barbar.SmartBlock
	Path string
}

func (b BatteryBlock) Run(c chan barbar.I3Block) error {
	parts := strings.Split(b.Path, "/")
	name := parts[len(parts)-1]

	now, e := os.Open(b.Path + "/energy_now")
	if e != nil {
		now, e = os.Open(b.Path + "/charge_now")
		if e != nil {
			return e
		}
	}
	full, e := os.Open(b.Path + "/energy_full")
	if e != nil {
		now, e = os.Open(b.Path + "/charge_full")
		if e != nil {
			return e
		}
	}

	pow, e := os.Open(b.Path + "/power_now")
	if e != nil {
		pow = nil
	}

	for {
		f, e := ioutil.ReadAll(full)
		if e != nil {
			time.Sleep(time.Minute)
			continue
		}
		n, e := ioutil.ReadAll(now)
		if e != nil {
			time.Sleep(time.Minute)
			continue
		}

		power := 0
		if pow != nil {
			p, e := ioutil.ReadAll(pow)
			if e == nil {
				power, _ = strconv.Atoi(strings.TrimSpace(string(p)))
			}
		}

		full_, e := strconv.Atoi(strings.TrimSpace(string(f)))
		if e != nil {
			continue
		}
		now_, e := strconv.Atoi(strings.TrimSpace(string(n)))
		if e != nil {
			continue
		}

		percent := float64(now_) / float64(full_) * 100
		str := fmt.Sprintf("%s: %5.2f%%", name, percent)
		if power != 0 {
			power := float64(power) / 1e6
			str += fmt.Sprintf(", %5.2fW", power)
		}

		c <- barbar.I3Block{
			FullText: str,
			Color:    batGrad(percent).String(),
		}

		time.Sleep(10 * time.Second)
		_, _ = full.Seek(0, 0)
		_, _ = now.Seek(0, 0)
		_, _ = pow.Seek(0, 0)
	}
}

func CreateBatteryBlocks() ([]BatteryBlock, error) {
	const batRoot = "/sys/class/power_supply"
	d, e := ioutil.ReadDir(batRoot)
	if e != nil {
		return nil, e
	}

	blks := make([]BatteryBlock, 0)

	for _, dir := range d {
		if len(dir.Name()) >= 3 && dir.Name()[:3] == "BAT" {
			blks = append(blks, BatteryBlock{
				Path: batRoot + "/" + dir.Name(),
			})
		}
	}
	return blks, nil
}
