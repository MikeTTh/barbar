package blocks

import (
	"fmt"
	"github.com/elastic/go-sysinfo"
	"gitlab.com/MikeTTh/barbar"
	"time"
)

type MemoryBlock struct {
	barbar.SmartBlock
	Change float64
}

func nearestUnit(base float64) string {
	near := base
	for _, unit := range []string{"KB", "MB", "GB", "TB"} {
		near /= 1024
		if near < 1024 {
			return fmt.Sprintf("%.2f%s", near, unit)
		}
	}

	return ""
}

func (m MemoryBlock) Run(c chan barbar.I3Block) error {
	if m.Change == 0 {
		m.Change = 1
	}

	lastAvail := uint64(0)

	for {
		host, err := sysinfo.Host()
		if err != nil {
			return err
		}
		mem, err := host.Memory()
		if err != nil {
			return err
		}

		change := float64(int(lastAvail) - int(mem.Available))
		changePercent := change / float64(mem.Total) * 100

		if changePercent > m.Change || changePercent < -m.Change {
			memPercent := float64(mem.Total-mem.Available) / float64(mem.Total) * 100.0

			c <- barbar.I3Block{
				FullText: fmt.Sprintf("Mem: %s/%s (%.2f%%)",
					nearestUnit(float64(mem.Total-mem.Available)),
					nearestUnit(float64(mem.Total)),
					memPercent,
				),
				Color: defaultGradient(memPercent).String(),
			}
			time.Sleep(time.Second / 10)
		} else {
			time.Sleep(time.Second * 2)
		}
		lastAvail = mem.Available
		time.Sleep(time.Second / 10)
	}
}
