package blocks

import (
	"gitlab.com/MikeTTh/barbar"
	"time"
)

type TimeBlock struct {
	barbar.SmartBlock
}

func (t TimeBlock) Run(c chan barbar.I3Block) error {
	for {
		date := time.Now().Format("Mon Jan 2 15:04:05")
		t := time.Now().Format("15:04:05")
		c <- barbar.I3Block{
			FullText:  date,
			ShortText: t,
		}
		time.Sleep(time.Second)
	}
}
