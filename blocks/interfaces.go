package blocks

import (
	"gitlab.com/MikeTTh/barbar"
	"net"
	"regexp"
	"strings"
	"time"
)

type InterfaceBlock struct {
	barbar.SmartBlock
	Name string
}

var linkLocalRegex = regexp.MustCompile("(fe80).*")
var validIfaceRegex = regexp.MustCompile("(en|wlp).*")

func (i InterfaceBlock) Run(c chan barbar.I3Block) error {
	iface, e := net.InterfaceByName(i.Name)
	if e != nil {
		return e
	}
	for {
		str := ""
		short := iface.Name
		shortHasIP := false
		addrs, e := iface.Addrs()
		if e != nil {
			return e
		}
		for _, a := range addrs {
			ipStr := strings.Split(a.String(), "/")[0]
			if linkLocalRegex.MatchString(ipStr) {
				continue
			}
			if !shortHasIP {
				short += " " + ipStr
				shortHasIP = true
			}
			str += ipStr + ", "
		}
		str = strings.TrimSuffix(str, ", ")

		name := iface.Name

		up := len(str) > 0
		color := red
		if up {
			color = green
			name += ": "
		}

		str = name + str

		c <- barbar.I3Block{
			FullText:  str,
			Color:     color.String(),
			ShortText: short,
		}

		time.Sleep(time.Second)
	}
}

func CreateIfaceBlocks() ([]InterfaceBlock, error) {
	ifs, e := net.Interfaces()
	if e != nil {
		return nil, e
	}

	blks := make([]InterfaceBlock, 0)

	for _, i := range ifs {
		if validIfaceRegex.MatchString(i.Name) {
			blks = append(blks, InterfaceBlock{
				Name: i.Name,
			})
		}
	}

	return blks, nil
}
