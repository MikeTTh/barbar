package barbar

type I3Header struct {
	Version     int  `json:"version"`
	StopSignal  int  `json:"stop_signal,omitempty"`
	ContSignal  int  `json:"cont_signal,omitempty"`
	ClickEvents bool `json:"click_events,omitempty"`
}

type I3Align string

const (
	Center I3Align = "center"
	Right  I3Align = "right"
	Left   I3Align = "left"
)

type I3Block struct {
	FullText            string  `json:"full_text"`
	ShortText           string  `json:"short_text,omitempty"`
	Color               string  `json:"color,omitempty"`
	Background          string  `json:"background,omitempty"`
	Border              string  `json:"border,omitempty"`
	BorderTop           int     `json:"border_top,omitempty"`
	BorderRight         int     `json:"border_right,omitempty"`
	BorderBottom        int     `json:"border_bottom,omitempty"`
	BorderLeft          int     `json:"border_left,omitempty"`
	MinWidthText        string  `json:"min_width,omitempty"`
	MinWidthPixel       int     `json:"min_width,omitempty"`
	Align               I3Align `json:"align,omitempty"`
	Name                string  `json:"name,omitempty"`
	Instance            string  `json:"instance,omitempty"`
	Urgent              bool    `json:"urgent,omitempty"`
	Separator           bool    `json:"separator,omitempty"`
	SeparatorBlockWidth int     `json:"separator_block_width,omitempty"`
	Markup              string  `json:"markup,omitempty"`
}
