package barbar

import (
	"encoding/json"
	"fmt"
	"gitlab.com/MikeTTh/graceful"
	"math/rand"
	"os"
	"time"
)

func init() {
	graceful.Listen(os.Interrupt)
}

type SmartListener struct {
	Header   I3Header
	Blocks   []SmartBlock
	channels []chan I3Block
	lastResp [][]byte
}

func (l *SmartListener) Listen() error {
	running := true
	by, e := json.Marshal(l.Header)
	if e != nil {
		return e
	}

	fmt.Println(string(by))

	fmt.Println("[")

	err := make(chan error)
	rerender := make(chan int)

	l.channels = make([]chan I3Block, len(l.Blocks))
	for i := range l.channels {
		l.channels[i] = make(chan I3Block)
	}
	l.lastResp = make([][]byte, len(l.Blocks))
	for i := range l.lastResp {
		l.lastResp[i] = []byte(`{"full_text":""}`)
	}

	graceful.AddHook(func(s os.Signal, done func()) {
		running = false
		err <- nil
		done()
	})

	go func() {
		for i, b := range l.Blocks {
			go func(i int, b SmartBlock) {
				err <- b.Run(l.channels[i])
			}(i, b)
			go func(i int) {
				for running {
					blk := <-l.channels[i]
					by, e := json.Marshal(blk)
					if e != nil {
						err <- e
					}
					l.lastResp[i] = by
					rerender <- i
				}
			}(i)
			r := rand.Float64()
			time.Sleep(time.Millisecond * time.Duration(r*1000))
		}
	}()

	go func() {
		for running {
			_ = <-rerender

			// clear rerender queue
			hasElems := true
			for hasElems {
				select {
				case _ = <-rerender:
				default:
					hasElems = false
				}
			}

			go func() {
				fmt.Println("[")
				for i, by := range l.lastResp {
					fmt.Print(string(by))
					if i != len(l.lastResp)-1 {
						fmt.Print(",")
					}
					fmt.Println()
				}
				fmt.Println("],")
			}()

			// limit redraws to 1/sec (swaybar went from 10% cpu to <=3%)
			// barbar only uses <2% even in fast mode
			time.Sleep(time.Second)
		}
	}()

	e = <-err
	running = false
	return e
}
