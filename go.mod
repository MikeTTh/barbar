module gitlab.com/MikeTTh/barbar

replace gitlab.com/MikeTTh/barbar => ./

go 1.15

require (
	github.com/elastic/go-sysinfo v1.5.0
	gitlab.com/MikeTTh/graceful v0.0.0-20210107133402-2d7e6b66efd9
)
