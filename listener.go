package barbar

import (
	"encoding/json"
	"fmt"
)

type I3Listener struct {
	Header  I3Header
	Channel chan []I3Block
}

func (l *I3Listener) Listen() error {
	b, e := json.Marshal(l.Header)
	if e != nil {
		return e
	}

	fmt.Println(string(b))

	fmt.Println("[")

	for {
		bar := <-l.Channel

		b, e = json.Marshal(bar)
		if e != nil {
			return e
		}
		fmt.Print(string(b))
		fmt.Println(",")
	}
}
