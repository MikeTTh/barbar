package main

import (
	"gitlab.com/MikeTTh/graceful"
	"runtime"
	"testing"
	"time"
)

func init() {
	runtime.SetCPUProfileRate(500)
}

func TestPerf(t *testing.T) {
	go func() {
		time.Sleep(time.Second * 20)
		graceful.RunHooks()
	}()
	main()
}
