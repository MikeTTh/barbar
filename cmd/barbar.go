package main

import (
	"gitlab.com/MikeTTh/barbar"
	"gitlab.com/MikeTTh/barbar/blocks"
	logger "log"
	"os"
)

func main() {
	logFile, e := os.Create("/tmp/barbar.log")
	if e != nil {
		panic(e)
	}
	log := logger.New(logFile, "barbar: ", 1)

	bats, e := blocks.CreateBatteryBlocks()
	if e != nil {
		log.Fatal(e)
	}
	var batblks = make([]barbar.SmartBlock, len(bats))
	for i, b := range bats {
		batblks[i] = b
	}

	ifs, e := blocks.CreateIfaceBlocks()
	if e != nil {
		log.Fatal(e)
	}
	var ifblks = make([]barbar.SmartBlock, len(ifs))
	for i, iface := range ifs {
		ifblks[i] = iface
	}

	blks := []barbar.SmartBlock{
		blocks.HanyadikhetvanBlock{},
	}

	blks = append(blks, ifblks...)

	blks = append(blks, batblks...)

	blks = append(
		blks,
		blocks.CpuBlock{},
		blocks.MemoryBlock{},
		blocks.TimeBlock{},
	)

	listen := barbar.SmartListener{
		Header: barbar.I3Header{
			Version: 1,
		},
		Blocks: blks,
	}

	e = listen.Listen()
	if e != nil {
		log.Fatal(e)
	}
}
