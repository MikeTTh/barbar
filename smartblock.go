package barbar

type SmartBlock interface {
	Run(c chan I3Block) error
}

type BasicSmartBlock struct {
	Function func() string
}

func (b *BasicSmartBlock) Run(c chan I3Block) error {
	for {
		str := b.Function()
		block := I3Block{
			FullText: str,
		}
		c <- block
	}
}
